format:
	@black main.py

check:
	@black main.py --check

test:
	@pytest -vvv -s


.PHONY: test check format