import os
from pathlib import Path
from unittest.mock import patch

from main import open_course_dir

basedir = Path(__file__).parent.resolve()


@patch.object(os, "listdir")
def test_open_course_dir_returns_1_file_inside_of_directory(mock_dir):
    mock_dir.return_value = ["1.yaml"]
    files = open_course_dir()
    assert len(files) == 1


@patch.object(os, "listdir")
def test_open_course_dir_returns_file_name(mock_dir):
    mock_dir.return_value = ["1.yaml"]
    files = open_course_dir()
    assert files[0] == "1.yaml"


def test_open_course_dir_ensure_that_be_FileNotFoundError():
    try:
        open_course_dir(basedir / "tmp")
    except FileNotFoundError as e:
        assert str(e) == "o diretório cursos não existe! 😑😞😑😞"
